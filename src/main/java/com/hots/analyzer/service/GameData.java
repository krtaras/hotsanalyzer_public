package com.hots.analyzer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hots.analyzer.bean.BornedUnit;
import com.hots.analyzer.bean.EndGameState;
import com.hots.analyzer.bean.HeroPlayer;
import com.hots.analyzer.bean.KilledByPlayers;
import com.hots.analyzer.bean.KilledUnit;
import com.hots.analyzer.bean.Level;
import com.hots.analyzer.bean.MQ;
import com.hots.analyzer.bean.PlayerInitData;
import com.hots.analyzer.bean.PlayersKill;
import com.hots.analyzer.bean.ReplayGameData;
import com.hots.analyzer.bean.Talent;
import com.hots.analyzer.bean.Team;

public class GameData {

	private static final String UnitDiedEvent = "UnitDiedEvent";
	private static final String PlayerSetupEvent = "PlayerSetupEvent";
	private static final String StatGameEvent = "StatGameEvent";
	private static final String UnitBornEvent = "UnitBornEvent";

	private static final String GameStartEventType = "GameStart";
	private static final String LevelUpEventType = "LevelUp";
	private static final String TalentChosenEventType = "TalentChosen";
	private static final String PlayerDeathEventType = "PlayerDeath";
	private static final String EndOfGameTalentChoicesEventType = "EndOfGameTalentChoices";

	private HashMap<Integer, List<KilledUnit>> killedUnitsByPlayers = new HashMap<Integer, List<KilledUnit>>();
	private HashMap<Integer, List<PlayersKill>> killedPlayers = new HashMap<Integer, List<PlayersKill>>();
	private HashMap<Integer, List<KilledByPlayers>> killedByPlayers = new HashMap<Integer, List<KilledByPlayers>>();
	private HashMap<Integer, List<BornedUnit>> bornedUnits = new HashMap<Integer, List<BornedUnit>>();
	private HashMap<Integer, List<Talent>> selectedTalents = new HashMap<Integer, List<Talent>>();
	private HashMap<Integer, List<Level>> levels = new HashMap<Integer, List<Level>>();
	private HashMap<Integer, Integer> playerSetup = new HashMap<Integer, Integer>();
	private HashMap<Integer, EndGameState> endGamePlayerStates = new HashMap<Integer, EndGameState>();
	private HashMap<Integer, PlayerInitData> playerInitData = new HashMap<Integer, PlayerInitData>();
	private int mapSizeX = -1;
	private int mapSizeY = -1;

	public GameData(JSONObject origin) throws JSONException {
		JSONArray m_playerList = origin.getJSONArray(MQ.D_PLAYER_LIST);
		for (int i = 0; i < m_playerList.length(); i++) {
			PlayerInitData initData = new PlayerInitData(m_playerList.getJSONObject(i));
			playerInitData.put(initData.getSlotId(), initData);
		}
	}

	public void processJSONObject(JSONObject origin) throws JSONException {
		String fullEventName = origin.getString(MQ.EVENT);
		fullEventName = fullEventName.replace("NNet.Replay.Tracker.S", "");
		switch (fullEventName) {
		case UnitDiedEvent: {
			addToKilledUnitsByPlayer(origin);
			break;
		}
		case UnitBornEvent: {
			addToUnitsBorn(origin);
			break;
		}
		case StatGameEvent: {
			processGameStats(origin);
			break;
		}
		case PlayerSetupEvent: {
			addToPlayerSetup(origin);
			break;
		}
		default:

		}
	}

	public ReplayGameData getGameData(String fileId) {

		Map<Integer, HeroPlayer> playersTeamOne = new HashMap<Integer, HeroPlayer>();
		Map<Integer, HeroPlayer> playersTeamTwo = new HashMap<Integer, HeroPlayer>();

		int teamId = -1;

		for (PlayerInitData data : playerInitData.values()) {
			int slotId = data.getSlotId();
			if (playerSetup.containsKey(slotId)) {
				int playerId = playerSetup.get(slotId);
				HeroPlayer player = new HeroPlayer(playerId, data.getLogin(), killedUnitsByPlayers.get(playerId),
						selectedTalents.get(playerId), levels.get(playerId), killedPlayers.get(playerId),
						killedByPlayers.get(playerId), endGamePlayerStates.get(playerId));
				if (teamId == -1) {
					teamId = data.getTeamId();
				}
				if (teamId == data.getTeamId()) {
					playersTeamOne.put(playerId, player);
				} else {
					playersTeamTwo.put(playerId, player);
				}
			}
		}

		Team teamOne = new Team(new ArrayList<HeroPlayer>(playersTeamOne.values()), "T1");
		Team teamTwo = new Team(new ArrayList<HeroPlayer>(playersTeamTwo.values()), "T2");

		HeroPlayer player = playersTeamOne.values().iterator().next();
		String mapName = player.getEndGameState().getMap();
		ReplayGameData gameData = new ReplayGameData(fileId, "", mapName, mapSizeX, mapSizeY, teamOne, teamTwo, bornedUnits);
		return gameData;
	}

	private void processGameStats(JSONObject origin) throws JSONException {
		String gameStatEvent = origin.getString(MQ.SUB_EVENT);
		switch (gameStatEvent) {
		case GameStartEventType: {
			setMapSize(origin);
			break;
		}
		case PlayerDeathEventType: {
			processPlayerDeathEvent(origin);
			break;
		}
		case LevelUpEventType: {
			addToLevel(origin);
			break;
		}
		case TalentChosenEventType: {
			addToTalent(origin);
			break;
		}
		case EndOfGameTalentChoicesEventType: {
			addEndGameState(origin);
			break;
		}
		default:
		}
	}

	private void addToKilledUnitsByPlayer(JSONObject object) throws JSONException {
		KilledUnit unit = new KilledUnit(object);
		if (!killedUnitsByPlayers.containsKey(unit.getPlayerId())) {
			killedUnitsByPlayers.put(unit.getPlayerId(), new ArrayList<KilledUnit>());
		}
		killedUnitsByPlayers.get(unit.getPlayerId()).add(unit);
	}

	private void addToUnitsBorn(JSONObject object) throws JSONException {
		BornedUnit unit = new BornedUnit(object);
		if (!bornedUnits.containsKey(unit.getPlayerId())) {
			bornedUnits.put(unit.getPlayerId(), new ArrayList<BornedUnit>());
		}
		bornedUnits.get(unit.getPlayerId()).add(unit);
	}
	
	private void addToPlayerSetup(JSONObject object) throws JSONException {
		int playerId = object.getInt(MQ.PLAYER_ID);
		int userId = object.getInt(MQ.USER_ID);
		playerSetup.put(userId, playerId);
	}

	private void processPlayerDeathEvent(JSONObject object) throws JSONException {
		KilledByPlayers killed = new KilledByPlayers(object);
		if (!killedByPlayers.containsKey(killed.getPlayerId())) {
			killedByPlayers.put(killed.getPlayerId(), new ArrayList<KilledByPlayers>());
		}
		killedByPlayers.get(killed.getPlayerId()).add(killed);

		JSONObject construct = new JSONObject();
		construct.put(MQ.ORDER, killed.getOrder());
		construct.put(MQ.FIXED_POSITION_X, killed.getPositionX());
		construct.put(MQ.FIXED_POSITION_Y, killed.getPositionY());
		construct.put(MQ.INT_PLAYER_ID, killed.getPlayerId());

		for (Integer killer : killed.getKillers()) {
			construct.put(MQ.INT_PLAYER_KILLER, killer);
			PlayersKill kill = new PlayersKill(construct);
			if (!killedPlayers.containsKey(kill.getPlayerId())) {
				killedPlayers.put(kill.getPlayerId(), new ArrayList<PlayersKill>());
			}
			killedPlayers.get(kill.getPlayerId()).add(kill);
		}
	}

	private void addToLevel(JSONObject object) throws JSONException {
		Level level = new Level(object);
		if (!levels.containsKey(level.getPlayerId())) {
			levels.put(level.getPlayerId(), new ArrayList<Level>());
		}
		levels.get(level.getPlayerId()).add(level);
	}

	private void addToTalent(JSONObject object) throws JSONException {
		Talent talent = new Talent(object);
		if (!selectedTalents.containsKey(talent.getPlayerId())) {
			selectedTalents.put(talent.getPlayerId(), new ArrayList<Talent>());
		}
		selectedTalents.get(talent.getPlayerId()).add(talent);
	}

	private void setMapSize(JSONObject object) throws JSONException {
		JSONArray m_fixedData = object.getJSONArray(MQ.FIXED_DATA);
		mapSizeX = MQ.getIntValueByKey(MQ.FIXED_MAP_SIZE_X, m_fixedData);
		mapSizeY = MQ.getIntValueByKey(MQ.FIXED_MAP_SIZE_Y, m_fixedData);
	}

	private void addEndGameState(JSONObject object) throws JSONException {
		EndGameState endGameState = new EndGameState(object);
		endGamePlayerStates.put(endGameState.getPlayerId(), endGameState);
	}
}
