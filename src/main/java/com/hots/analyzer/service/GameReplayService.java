package com.hots.analyzer.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hots.analyzer.bean.ReplayGameData;
import com.hots.analyzer.bean.ReplayGameDataDetails;
import com.hots.analyzer.bean.ReplayGamePreview;
import com.hots.analyzer.repository.ReplayDataPreviewRepository;
import com.hots.analyzer.repository.ReplayGameDataDetailsRepository;
import com.hots.analyzer.repository.ReplayGameDataRepository;

@Service
public class GameReplayService {
	
	@Autowired
	private ReplayGameDataRepository repository;
	
	@Autowired
	private ReplayGameDataDetailsRepository dataDetailsRepository;
	
	@Autowired
	private ReplayDataPreviewRepository dataPreviewRepository;
	
	private static final String TRACKEREVENTS ="trackerevents";
	private static final String STATS ="stats";
	private static final String GAMEEVENTS ="gameevents";
	private static final String HEADER = "header";
	private static final String DETAILS ="details";
	
	private static final String EXECUTE_COMMAND = "C:/Python27/python E:/Work/WorkspaceSTS/HotsAnalyzer/heroprotocol/heroprotocol.py --%c --json \"E:/Work/UploadFolder/%f\"";
	private static final String UPLOAD_DIR = "E:/Work/UploadFolder/";

	/*private static final String EXECUTE_COMMAND = "C:/Python27/python D:/MyLibrary/sts-workspace/HotsAnalyzer/heroprotocol/heroprotocol.py --%c --json \"D:/MyLibrary/uploadedFiles/%f\"";
	private static final String UPLOAD_DIR = "D:/MyLibrary/uploadedFiles/";
*/	
	public ReplayGameData uploadFile(MultipartFile uploadedFile) throws Exception {
		String fileName = saveFileToFolder(uploadedFile);
		GameData gameData = convert(fileName);
		ReplayGameData toInsert = gameData.getGameData(fileName);
		GameDataDetails details = getDataDetails(fileName);
		ReplayGameDataDetails dataDetails = details.getReplayDetails(fileName);
		toInsert.setGameDataDetails(dataDetails);
		repository.insert(toInsert);
        return toInsert;
	}
	
	public BufferedReader getFileBuffer(String fileName) {
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(UPLOAD_DIR + fileName + ".json"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return bufferedReader;
	}
	
	public ReplayGameData getReplayDataById(String id) {
		return repository.findOne(id);
	}
	
	private String saveFileToFolder(MultipartFile uploadedFile) throws IllegalStateException, IOException {
		long fileId = new Date().getTime();
    	String fileName = Long.toString(fileId);
		File file = new File(UPLOAD_DIR + fileName);
        uploadedFile.transferTo(file);
        return fileName;
	}
	
	private GameData convert(String fileName) throws IOException, JSONException {
		GameData gameData = null;
		BufferedReader stdInput;
		Process p;
		String command = EXECUTE_COMMAND.replace("%f", fileName);
       
		p = Runtime.getRuntime().exec(command.replace("%c", DETAILS));
        stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String s = null;
        if ((s = stdInput.readLine()) != null) {
        	JSONObject origin = new JSONObject(s);
        	gameData = new GameData(origin);
        }

        if (gameData != null) {
	        p = Runtime.getRuntime().exec(command.replace("%c", TRACKEREVENTS));
	        stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
	        while ((s = stdInput.readLine()) != null) {
	        	JSONObject origin = new JSONObject(s);
	        	gameData.processJSONObject(origin);
			}
        }
        
        return gameData;
	}
	
	public GameDataDetails getDataDetails(String fileName) throws IOException, JSONException {
		String command = EXECUTE_COMMAND.replace("%f", fileName);
		Process p = Runtime.getRuntime().exec(command.replace("%c", GAMEEVENTS));
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String s = null;
		GameDataDetails details = new GameDataDetails();
		while ((s = stdInput.readLine()) != null) {
        	JSONObject origin = new JSONObject(s);
        	details.processJSONObject(origin);
		}
		return details;
	}
	
	public List<ReplayGamePreview> getAllUserGame(String userId) {
		List<ReplayGamePreview> result = new ArrayList<ReplayGamePreview>();
		result.addAll(dataPreviewRepository.findAllByUserId(userId));
		return result;
	}
	
	public void saveRamePreview(ReplayGameData replayGameData, String userId) {
		ReplayGamePreview preview = new ReplayGamePreview();
		preview.setMapName(replayGameData.getMapName());
		preview.setReplayDataId(replayGameData.getId());
		preview.setUserId(userId);
		preview.setName(replayGameData.getName());
		dataPreviewRepository.insert(preview);
	}
}
