package com.hots.analyzer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hots.analyzer.bean.User;
import com.hots.analyzer.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	public boolean isUser(String login, String password) {
		return getUser(login, password) != null;
	}
	
	public void createUser(String login, String password) {
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		repository.insert(user);
	}
	
	public User getUser(String login, String password) {
		return repository.findOneByLoginAndPassword(login, password);
	}
}
