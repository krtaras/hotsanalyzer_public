package com.hots.analyzer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.hots.analyzer.bean.HeroPlayerDetails;
import com.hots.analyzer.bean.MQ;
import com.hots.analyzer.bean.ReplayGameDataDetails;
import com.hots.analyzer.bean.TargetPoint;

public class GameDataDetails {
	private static final String TargetPointEvent = "UpdateTargetPointEvent";
	
	private HashMap<Integer, List<TargetPoint>> playersMovePoints = new HashMap<Integer, List<TargetPoint>>();
	
	public void processJSONObject(JSONObject origin) throws JSONException {
		String fullEventName = origin.getString(MQ.EVENT);
		fullEventName = fullEventName.replace("NNet.Game.SCmd", "");
		switch (fullEventName) {
		case TargetPointEvent: {
			addToTargetEventPoint(origin);
			break;
		}
		default: {
			break;
		}
		}
	}
	
	public void addToTargetEventPoint(JSONObject origin) throws JSONException {
		TargetPoint point = new TargetPoint(origin);
		if (!playersMovePoints.containsKey(point.getPlayerId())) {
			playersMovePoints.put(point.getPlayerId(), new ArrayList<TargetPoint>());
		}
		playersMovePoints.get(point.getPlayerId()).add(point);
	}
	
	public ReplayGameDataDetails getReplayDetails(String fileName) {
		List<HeroPlayerDetails> heroes = new ArrayList<HeroPlayerDetails>();
		for (Integer id : playersMovePoints.keySet()) {
			HeroPlayerDetails details = new HeroPlayerDetails();
			details.setMovePoints(playersMovePoints.get(id));
			details.setPlayerId(id);
			heroes.add(details);
		}
		ReplayGameDataDetails gameDataDetails = new ReplayGameDataDetails();
		gameDataDetails.setFileId(fileName);
		gameDataDetails.setPlayers(heroes);
		return gameDataDetails;
	}
	
	private int getMaxSize() {
		int max = 0;
		for (List<TargetPoint> points : playersMovePoints.values()) {
			if (points.get(points.size()-1).getOrder() > max) {
				max = points.get(points.size()-1).getOrder();
			}
		}
		return max;
	}
	
	private void normalizeMovePoints(int max) {
		for (int i = 0; i < max; i++) {
			
		}
	}
}
