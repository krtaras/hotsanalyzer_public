package com.hots.analyzer.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hots.analyzer.bean.User;
import com.hots.analyzer.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> login(@RequestBody User user) {
		System.out.println(new Date());
		try {
			if (userService.isUser(user.getLogin(), user.getPassword())) {
				user = userService.getUser(user.getLogin(), user.getPassword());
				user.setPassword("");
				ResponseEntity<User> responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);
				return responseEntity;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<User>(HttpStatus.FAILED_DEPENDENCY);
	}
	
	@RequestMapping(value = "/register", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> register(@RequestBody User user) {
		System.out.println(new Date());
		try {
			if (!userService.isUser(user.getLogin(), user.getPassword())) {
				userService.createUser(user.getLogin(), user.getPassword());
				ResponseEntity<User> responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);
				return responseEntity;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<User>(HttpStatus.FAILED_DEPENDENCY);
	}
}
