package com.hots.analyzer.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hots.analyzer.bean.ReplayGameData;
import com.hots.analyzer.bean.Response;
import com.hots.analyzer.service.GameReplayService;

@Controller
@RequestMapping("/filemamager")
public class FileUploadController {

	@Autowired
	private GameReplayService fileService;

	@RequestMapping("/")
	public String uploadPage() {
		return "uploadPage";
	}

	@RequestMapping("/upload")
	@ResponseBody
	public ResponseEntity<Response> upload(@RequestParam("file") MultipartFile uploadedFile, @RequestParam("user") String user) {
		System.out.println(new Date());
		System.out.println(user);
		try {
			ReplayGameData data = fileService.uploadFile(uploadedFile);
			if (user != null && !user.isEmpty()) {
				fileService.saveRamePreview(data, user);
			}
			Response response = new Response();
			response.setBody(data.getId());
			ResponseEntity<Response> responseEntity = new ResponseEntity<Response>(response, HttpStatus.OK);
			return responseEntity;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Response>(HttpStatus.FAILED_DEPENDENCY);
	}
}
