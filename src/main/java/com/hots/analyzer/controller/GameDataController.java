package com.hots.analyzer.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hots.analyzer.bean.ReplayGameData;
import com.hots.analyzer.bean.ReplayGamePreview;
import com.hots.analyzer.bean.User;
import com.hots.analyzer.service.GameReplayService;

@Controller
@RequestMapping("/game")
public class GameDataController {
	
	@Autowired
	private GameReplayService gameReplayService;
	
	@RequestMapping(value="/", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<ReplayGamePreview>> myGameList(@RequestBody User user) {
		System.out.println(new Date());
		try {
			if (user != null && !user.getId().isEmpty()) {
				List<ReplayGamePreview> data = gameReplayService.getAllUserGame(user.getId());
				ResponseEntity<List<ReplayGamePreview>> responseEntity = new ResponseEntity<List<ReplayGamePreview>>(data, HttpStatus.OK);
				return responseEntity;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<ReplayGamePreview>>(HttpStatus.FAILED_DEPENDENCY);
	}
	
	@RequestMapping("/{id}")
	@ResponseBody
	public ResponseEntity<ReplayGameData> upload(@PathVariable String id) {
		System.out.println(new Date());
		try {
			ReplayGameData data = gameReplayService.getReplayDataById(id);
			ResponseEntity<ReplayGameData> responseEntity = new ResponseEntity<ReplayGameData>(data, HttpStatus.OK);
			return responseEntity;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ReplayGameData>(HttpStatus.FAILED_DEPENDENCY);
	}
}
