package com.hots.analyzer.bean;

import java.util.List;

public class Team {

	private List<HeroPlayer> players;
	private String name;

	public Team() {
		super();
	}
	
	public Team(List<HeroPlayer> players, String name) {
		super();
		this.players = players;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<HeroPlayer> getPlayers() {
		return players;
	}

	public void setPlayers(List<HeroPlayer> players) {
		this.players = players;
	}

	public void setName(String name) {
		this.name = name;
	}

}
