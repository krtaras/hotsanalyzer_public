package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class Bean {
	
	protected int order = -1;
	protected int playerId = -1;

	public Bean() {
		super();
	}

	public Bean(int order, int playerId) {
		super();
		this.order = order;
		this.playerId = playerId;
	}

	public Bean(JSONObject object) throws JSONException {
		this.order = object.getInt(MQ.ORDER);
	}
	
	public int getPlayerId() {
		return playerId;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	
}
