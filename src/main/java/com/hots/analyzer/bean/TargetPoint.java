package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class TargetPoint extends Bean {

	private int positionX;
	private int positionY;
	
	public TargetPoint() {
	}
	
	public TargetPoint(JSONObject object) throws JSONException {
		super(object);
		this.playerId = object.getJSONObject("_userid").getInt("m_userId");
		this.positionX = object.getJSONObject("m_target").getInt("x");
		this.positionY = object.getJSONObject("m_target").getInt("y");
	}
	
	public int getPositionX() {
		return positionX;
	}
	public int getPositionY() {
		return positionY;
	}
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
}
