package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class KilledUnit extends Bean {

	private int x;
	private int y;
	
	public KilledUnit() {
		super();
	}
	
	public KilledUnit(int order, int playerId, int x, int y) {
		super(order, playerId);
		this.x = x;
		this.y = y;
	}

	public KilledUnit(JSONObject object) throws JSONException {
		super(object);
		this.x = object.getInt(MQ.X);
		this.y = object.getInt(MQ.Y);
		if (!object.isNull(MQ.UNIT_KILLER)) {
			this.playerId = object.getInt(MQ.UNIT_KILLER);
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
}
