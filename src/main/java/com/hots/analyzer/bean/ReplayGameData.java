package com.hots.analyzer.bean;

import java.util.HashMap;
import java.util.List;

import org.springframework.data.annotation.Id;

public class ReplayGameData {

	@Id
	private String id;
	private String fileId;
	private String name;
	private String mapName;

	private int maxSizeX;
	private int maxSizeY;

	private Team teamOne;
	private Team teamTwo;

	private HashMap<Integer, List<BornedUnit>> bornedUnits;
	private ReplayGameDataDetails gameDataDetails;
	
	public ReplayGameData(String fileId, String name, String mapName, int maxSizeX, int maxSizeY, Team teamOne,
			Team teamTwo, HashMap<Integer, List<BornedUnit>> bornedUnits) {
		super();
		this.fileId = fileId;
		this.name = name;
		this.mapName = mapName;
		this.maxSizeX = maxSizeX;
		this.maxSizeY = maxSizeY;
		this.teamOne = teamOne;
		this.teamTwo = teamTwo;
		this.bornedUnits = bornedUnits;
	}

	public String getId() {
		return id;
	}

	public String getFileId() {
		return fileId;
	}

	public String getName() {
		return name;
	}

	public String getMapName() {
		return mapName;
	}

	public int getMaxSizeX() {
		return maxSizeX;
	}

	public int getMaxSizeY() {
		return maxSizeY;
	}

	public Team getTeamOne() {
		return teamOne;
	}

	public Team getTeamTwo() {
		return teamTwo;
	}

	public HashMap<Integer, List<BornedUnit>> getBornedUnits() {
		return bornedUnits;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public void setMaxSizeX(int maxSizeX) {
		this.maxSizeX = maxSizeX;
	}

	public void setMaxSizeY(int maxSizeY) {
		this.maxSizeY = maxSizeY;
	}

	public void setTeamOne(Team teamOne) {
		this.teamOne = teamOne;
	}

	public void setTeamTwo(Team teamTwo) {
		this.teamTwo = teamTwo;
	}

	public void setBornedUnits(HashMap<Integer, List<BornedUnit>> bornedUnits) {
		this.bornedUnits = bornedUnits;
	}

	public ReplayGameDataDetails getGameDataDetails() {
		return gameDataDetails;
	}

	public void setGameDataDetails(ReplayGameDataDetails gameDataDetails) {
		this.gameDataDetails = gameDataDetails;
	}
	
}
