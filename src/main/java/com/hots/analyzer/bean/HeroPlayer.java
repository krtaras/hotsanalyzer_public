package com.hots.analyzer.bean;

import java.util.List;

import org.springframework.data.annotation.Id;

public class HeroPlayer {

	@Id
	private int id;
	private int playerId;
	private String login;
	private String hero;

	private List<KilledUnit> killedUnits;
	private List<Talent> selectedTalents;
	private List<Level> level;
	private List<PlayersKill> killedPlayer;
	private List<KilledByPlayers> killedByPlayers;
	private EndGameState endGameState;
	
	public HeroPlayer() {
		super();
	}

	public HeroPlayer(int id, int playerId, String login, String hero, List<KilledUnit> killedUnits,
			List<Talent> selectedTalents, List<Level> level, List<PlayersKill> killedPlayer,
			List<KilledByPlayers> killedByPlayers, EndGameState endGameState) {
		super();
		this.id = id;
		this.playerId = playerId;
		this.login = login;
		this.hero = hero;
		this.killedUnits = killedUnits;
		this.selectedTalents = selectedTalents;
		this.level = level;
		this.killedPlayer = killedPlayer;
		this.killedByPlayers = killedByPlayers;
		this.endGameState = endGameState;
	}

	public HeroPlayer(int playerId, String login, List<KilledUnit> killedUnits,
			List<Talent> selectedTalents, List<Level> level, List<PlayersKill> killedPlayer,
			List<KilledByPlayers> killedByPlayers, EndGameState endGameState) {
		super();
		this.playerId = playerId;
		this.login = login;
		this.hero = endGameState.getHero();
		this.killedUnits = killedUnits;
		this.selectedTalents = selectedTalents;
		this.level = level;
		this.killedPlayer = killedPlayer;
		this.killedByPlayers = killedByPlayers;
		this.endGameState = endGameState;
	}

	public int getPlayerId() {
		return playerId;
	}

	public String getLogin() {
		return login;
	}

	public String getHero() {
		return hero;
	}

	public List<KilledUnit> getKilledUnits() {
		return killedUnits;
	}

	public List<Talent> getSelectedTalents() {
		return selectedTalents;
	}

	public List<Level> getLevel() {
		return level;
	}

	public List<PlayersKill> getKilledPlayer() {
		return killedPlayer;
	}

	public List<KilledByPlayers> getKilledByPlayers() {
		return killedByPlayers;
	}

	public int getId() {
		return id;
	}

	public EndGameState getEndGameState() {
		return endGameState;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setHero(String hero) {
		this.hero = hero;
	}

	public void setKilledUnits(List<KilledUnit> killedUnits) {
		this.killedUnits = killedUnits;
	}

	public void setSelectedTalents(List<Talent> selectedTalents) {
		this.selectedTalents = selectedTalents;
	}

	public void setLevel(List<Level> level) {
		this.level = level;
	}

	public void setKilledPlayer(List<PlayersKill> killedPlayer) {
		this.killedPlayer = killedPlayer;
	}

	public void setKilledByPlayers(List<KilledByPlayers> killedByPlayers) {
		this.killedByPlayers = killedByPlayers;
	}

	public void setEndGameState(EndGameState endGameState) {
		this.endGameState = endGameState;
	}
}
