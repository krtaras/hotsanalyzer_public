package com.hots.analyzer.bean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Level extends Bean {

	private int level;
	
	public Level() {
		super();
	}
	
	public Level(int order, int playerId, int level) {
		super(order, playerId);
		this.level = level;
	}

	public Level(JSONObject object) throws JSONException {
		super(object);
		JSONArray m_intData = object.getJSONArray(MQ.INT_DATA);
		this.level = MQ.getIntValueByKey(MQ.LEVEL, m_intData);
		this.playerId = MQ.getIntValueByKey(MQ.INT_PLAYER_ID, m_intData);
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

}
