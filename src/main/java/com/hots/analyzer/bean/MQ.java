package com.hots.analyzer.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MQ {

	private static final String KEY = "m_key";
	private static final String VALUE = "m_value";
	
	public static final String USER_ID = "m_userId";
	public static final String EVENT = "_event";
	public static final String SUB_EVENT = "m_eventName";
	public static final String ORDER = "_gameloop";
	public static final String STRING_DATA = "m_stringData";
	public static final String INT_DATA = "m_intData";
	public static final String INT_PLAYER_ID = "PlayerID";
	public static final String LEVEL = "Level";
	public static final String TALENT = "PurchaseName";
	public static final String X = "m_x";
	public static final String Y = "m_y";
	public static final String UPKEEP_PLAYER = "m_upkeepPlayerId";
	public static final String UNIT_KILLER = "m_killerPlayerId";
	public static final String INT_PLAYER_KILLER = "KillingPlayer";
	public static final String FIXED_DATA = "m_fixedData";
	public static final String FIXED_POSITION_X = "PositionX";
	public static final String FIXED_POSITION_Y = "PositionY";
	public static final String PLAYER_ID = "m_playerId";
	public static final String SLOT_ID = "m_slotId";
	public static final String FIXED_MAP_SIZE_X = "MapSizeX";
	public static final String FIXED_MAP_SIZE_Y = "MapSizeY";
	public static final String STRING_WIN_LOSS = "Win/Loss";
	public static final String STRING_MAP = "Map";
	public static final String STRING_HERO = "Hero";
	public static final String D_PLAYER_LIST = "m_playerList";
	public static final String D_SLOT_ID = "m_workingSetSlotId";
	public static final String D_TEAM_ID = "m_teamId";
	public static final String D_LOGIN = "m_name";
	public static final String D_RESULT_ID = "m_result";
	public static final String D_COLOR = "m_color";
	
	
	public static String getValueByKey(String key, JSONArray items) throws JSONException {
		for (int i = 0; i < items.length(); i++) {
			JSONObject item = items.getJSONObject(i);
			if (item.getString(KEY).equalsIgnoreCase(key)) {
				return item.getString(VALUE);
			}
		}
		return "";
	}
	
	public static int getIntValueByKey(String key, JSONArray items) throws JSONException {
		String value = getValueByKey(key, items);
		if (value.isEmpty()) {
			return -1;
		}
		return Integer.parseInt(value);
	}
	
	public static List<String> getListValuesByKey(String key, JSONArray items) throws JSONException {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < items.length(); i++) {
			JSONObject item = items.getJSONObject(i);
			if (item.getString(KEY).equalsIgnoreCase(key)) {
				result.add(item.getString(VALUE));
			}
		}
		return result;
	}
	
	public static List<Integer> getIntListValuesByKey(String key, JSONArray items) throws JSONException {
		List<Integer> result = new ArrayList<Integer>();
		for (String value : getListValuesByKey(key, items)) {
			try {
				result.add(Integer.parseInt(value));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
