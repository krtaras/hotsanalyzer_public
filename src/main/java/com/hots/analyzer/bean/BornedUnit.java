package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class BornedUnit extends Bean {

	private int x;
	private int y;
	
	public BornedUnit() {
		super();
	}
	
	public BornedUnit(int order, int playerId, int x, int y) {
		super(order, playerId);
		this.x = x;
		this.y = y;
	}

	public BornedUnit(JSONObject object) throws JSONException {
		super(object);
		this.x = object.getInt(MQ.X);
		this.y = object.getInt(MQ.Y);
		if (!object.isNull(MQ.UPKEEP_PLAYER)) {
			this.playerId = object.getInt(MQ.UPKEEP_PLAYER);
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
}
