package com.hots.analyzer.bean;

import java.util.List;

import org.springframework.data.annotation.Id;

public class HeroPlayerDetails {

	@Id
	private int id;
	private int playerId;
	private List<TargetPoint> movePoints;

	public int getId() {
		return id;
	}

	public int getPlayerId() {
		return playerId;
	}

	public List<TargetPoint> getMovePoints() {
		return movePoints;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public void setMovePoints(List<TargetPoint> movePoints) {
		this.movePoints = movePoints;
	}

}
