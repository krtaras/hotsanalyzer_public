package com.hots.analyzer.bean;

import java.util.List;

import org.springframework.data.annotation.Id;

public class ReplayGameDataDetails {

	@Id
	private String id;
	private String fileId;
	private List<HeroPlayerDetails> players;
	
	public String getId() {
		return id;
	}
	public String getFileId() {
		return fileId;
	}
	public List<HeroPlayerDetails> getPlayers() {
		return players;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public void setPlayers(List<HeroPlayerDetails> players) {
		this.players = players;
	}
}
