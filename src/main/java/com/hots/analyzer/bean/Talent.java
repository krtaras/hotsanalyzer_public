package com.hots.analyzer.bean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Talent extends Bean {

	private String talent;
	
	public Talent() {
		super();
	}
	
	public Talent(int order, int playerId, String talent) {
		super(order, playerId);
		this.talent = talent;
	}

	public Talent(JSONObject object) throws JSONException {
		super(object);
		JSONArray m_intData = object.getJSONArray(MQ.INT_DATA);
		this.playerId = MQ.getIntValueByKey(MQ.INT_PLAYER_ID, m_intData);
		JSONArray m_stringData = object.getJSONArray(MQ.STRING_DATA);
		this.talent = MQ.getValueByKey(MQ.TALENT, m_stringData);
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}
}
