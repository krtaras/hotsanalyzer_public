package com.hots.analyzer.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ReplayGamePreview {

	@Id
	private String id;
	private String userId;
	private String name;
	private String mapName;
	private String replayDataId;

	public String getName() {
		return name;
	}

	public String getMapName() {
		return mapName;
	}

	public String getReplayDataId() {
		return replayDataId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public void setReplayDataId(String replayDataId) {
		this.replayDataId = replayDataId;
	}

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
