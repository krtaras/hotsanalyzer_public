package com.hots.analyzer.bean;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class KilledByPlayers extends Bean {

	private Set<Integer> killers;
	private int positionX;
	private int positionY;
	
	public KilledByPlayers() {
		super();
	}
	
	public KilledByPlayers(int order, int playerId, Set<Integer> killers, int positionX, int positionY) {
		super(order, playerId);
		this.killers = killers;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public KilledByPlayers(JSONObject object) throws JSONException {
		super(object);
		JSONArray m_intData = object.getJSONArray(MQ.INT_DATA);
		this.playerId = MQ.getIntValueByKey(MQ.INT_PLAYER_ID, m_intData);
		this.killers = new HashSet<Integer>(MQ.getIntListValuesByKey(MQ.INT_PLAYER_KILLER, m_intData));
		JSONArray m_fixedData = object.getJSONArray(MQ.FIXED_DATA);
		this.positionX = MQ.getIntValueByKey(MQ.FIXED_POSITION_X, m_fixedData);
		this.positionY = MQ.getIntValueByKey(MQ.FIXED_POSITION_Y, m_fixedData);
	}
	
	public Set<Integer> getKillers() {
		return killers;
	}

	public int getPositionX() {
		return positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setKillers(Set<Integer> killers) {
		this.killers = killers;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
}
