package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class PlayerInitData {

	private int slotId = -1;
	private int teamId = -1;
	private String login = "";
	private int resultId = -1;
	private String color = "";

	public PlayerInitData() {
		super();
	}

	public PlayerInitData(int slotId, int teamId, String login, int resultId, String color) {
		super();
		this.slotId = slotId;
		this.teamId = teamId;
		this.login = login;
		this.resultId = resultId;
		this.color = color;
	}

	public PlayerInitData(JSONObject object) throws JSONException {
		this.slotId = object.getInt(MQ.D_SLOT_ID);
		this.teamId = object.getInt(MQ.D_TEAM_ID);
		this.login = object.getString(MQ.D_LOGIN);
		this.resultId = object.getInt(MQ.D_RESULT_ID);
		this.color = getColor(object.getJSONObject(MQ.D_COLOR));
	}

	private String getColor(JSONObject object) throws JSONException {
		int r = object.getInt("m_r");
		int g = object.getInt("m_g");
		int b = object.getInt("m_b");
		int a = object.getInt("m_a");
		return String.format("#%02x%02x%02x%02x", a, r, g, b);
	}

	public int getSlotId() {
		return slotId;
	}

	public int getTeamId() {
		return teamId;
	}

	public String getLogin() {
		return login;
	}

	public int getResultId() {
		return resultId;
	}

	public String getColor() {
		return color;
	}

	public void setSlotId(int slotId) {
		this.slotId = slotId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
