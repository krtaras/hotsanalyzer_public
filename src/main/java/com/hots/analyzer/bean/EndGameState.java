package com.hots.analyzer.bean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EndGameState extends Bean {

	private String map;
	private String result;
	private String hero;
	private int level;
	
	public EndGameState() {
		super();
	}
	
	public EndGameState(int order, int playerId, String map, String result, String hero, int level) {
		super(order, playerId);
		this.map = map;
		this.result = result;
		this.hero = hero;
		this.level = level;
	}

	public EndGameState(JSONObject object) throws JSONException {
		super(object);
		JSONArray m_intData = object.getJSONArray(MQ.INT_DATA);
		this.level = MQ.getIntValueByKey(MQ.LEVEL, m_intData);
		this.playerId = MQ.getIntValueByKey(MQ.INT_PLAYER_ID, m_intData);

		JSONArray m_stringData = object.getJSONArray(MQ.STRING_DATA);
		this.result = MQ.getValueByKey(MQ.STRING_WIN_LOSS, m_stringData);
		this.map = MQ.getValueByKey(MQ.STRING_MAP, m_stringData);
		this.hero = MQ.getValueByKey(MQ.STRING_HERO, m_stringData);
	}

	public String getMap() {
		return map;
	}

	public String getResult() {
		return result;
	}

	public String getHero() {
		return hero;
	}

	public int getLevel() {
		return level;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setHero(String hero) {
		this.hero = hero;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
