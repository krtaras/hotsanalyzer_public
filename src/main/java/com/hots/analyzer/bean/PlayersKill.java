package com.hots.analyzer.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class PlayersKill extends Bean {

	public PlayersKill() {
		super();
	}

	public PlayersKill(int order, int playerId, int killedPlayer, int positionX, int positionY) {
		super(order, playerId);
		this.killedPlayer = killedPlayer;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public PlayersKill(JSONObject object) throws JSONException {
		super(object);
		this.playerId = object.getInt(MQ.INT_PLAYER_KILLER);
		this.positionX = object.getInt(MQ.FIXED_POSITION_X);
		this.positionY = object.getInt(MQ.FIXED_POSITION_Y);
		this.killedPlayer = object.getInt(MQ.INT_PLAYER_ID);
	}

	private int killedPlayer;
	private int positionX;
	private int positionY;

	public int getKilledPlayer() {
		return killedPlayer;
	}

	public int getPositionX() {
		return positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setKilledPlayer(int killedPlayer) {
		this.killedPlayer = killedPlayer;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

}
