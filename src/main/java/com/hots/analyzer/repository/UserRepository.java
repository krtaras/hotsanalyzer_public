package com.hots.analyzer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hots.analyzer.bean.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findOneByLoginAndPassword(String login, String password);	
}
