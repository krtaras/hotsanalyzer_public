package com.hots.analyzer.repository;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hots.analyzer.bean.ReplayGamePreview;

public interface ReplayDataPreviewRepository extends MongoRepository<ReplayGamePreview, String> {

	Collection<? extends ReplayGamePreview> findAllByUserId(String userId);

}
