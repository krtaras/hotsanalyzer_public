package com.hots.analyzer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hots.analyzer.bean.ReplayGameData;

public interface ReplayGameDataRepository extends MongoRepository<ReplayGameData, String> {

}
