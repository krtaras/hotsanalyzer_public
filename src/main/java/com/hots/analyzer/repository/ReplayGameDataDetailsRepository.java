package com.hots.analyzer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hots.analyzer.bean.ReplayGameDataDetails;

public interface ReplayGameDataDetailsRepository extends MongoRepository<ReplayGameDataDetails, String> {

}
