(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .service('UserAuthenticationService', UserAuthenticationService);

    UserAuthenticationService.$inject = ['$http'];
    function UserAuthenticationService($http) {
        var loginedUser = null;
        var vm = this;
        vm.signIn = signIn;
        vm.signUp = signUp;
        vm.signOut = signOut;
        vm.isAuthorized = isAuthorized;
        vm.getCurrentUser = getCurrentUser;
    	
    	function isAuthorized() {
    		return vm.loginedUser != null; 
    	}
    	
        function signIn(login, password, callback) { 
        	$http.post('user/login', {
        		login: login,
        		password: password
        	}).then(function(response) {
        		vm.loginedUser = response.data;
            	callback();
            });
        }
        
        function signUp(login, password, callback) { 
        	$http.post('user/register', {
        		login: login,
        		password: password
        	}).then(function(response) {
        		vm.loginedUser = response.data;
            	callback();
            });
        }
        
        function signOut(callback) {
        	vm.loginedUser = null;
        	callback();
        }
        
        function getCurrentUser() {
        	return vm.loginedUser;
        }
    }
})();