(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .controller('UserController', UserController);

    UserController.$inject = ['UserAuthenticationService'];
    function UserController(UserAuthenticationService) {
        var vm = this;
        
        vm.isLoginDialog = true;
        vm.logginned = UserAuthenticationService.isAuthorized();
        vm.currentUser = UserAuthenticationService.getCurrentUser();
        vm.form = getEmptyForm();
        
        vm.signIn = signIn;
        vm.signUp = signUp;
        vm.signOut = signOut;
        vm.login = login;
        vm.registration = registration;
        
        vm.myPage = myPage;
        
        function myPage() {
        	$state.go('mypage');
        }
        
        function signIn() {
        	vm.isLoginDialog = true;
        	vm.form = getEmptyForm();
        	toggleDialog();
        }
        
        function signUp() {
        	vm.isLoginDialog = false;
        	vm.form = getEmptyForm();
        	toggleDialog();
        }
        
        function signOut() {
        	UserAuthenticationService.signOut(refresh);
		}
        
        function toggleDialog() {
        	$('#userDialog').modal("toggle"); 
        }
        
        function login() {
        	UserAuthenticationService.signIn(vm.form.login, vm.form.password, refreshAndClose);
        }
        
        function registration() {
        	UserAuthenticationService.signUp(vm.form.login, vm.form.password, refreshAndClose);
        }
        
        function refresh() {
        	vm.logginned = UserAuthenticationService.isAuthorized();
        	vm.currentUser = UserAuthenticationService.getCurrentUser();
        	vm.form = getEmptyForm();
        }
        
        function refreshAndClose() {
        	refresh();
        	toggleDialog();
        }
        
		function getEmptyForm() {
			return {
	        	login: '',
	        	password: ''
	        } 
		}
    }
})();