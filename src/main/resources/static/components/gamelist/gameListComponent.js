(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .component('gameListComponent', {
            templateUrl: 'components/gamelist/gameListTemplate.html',
            controller: GameListController,
            bindings: {
            },
        });

    GameListController.$inject = ['$http', 'UserAuthenticationService'];
    function GameListController($http, UserAuthenticationService) {
        var $ctrl = this;
        $ctrl.games = [];
        $ctrl.$onInit = getGameList;
        $ctrl.getMapURL = getMapURL;
        
        function getGameList() {
        	$http.post('game/',UserAuthenticationService.getCurrentUser()).then(function(response) {
        		$ctrl.games = response.data;
        		console.log($ctrl.games);
            });
        }
        
        function getMapURL(mapName) {
        	return "media/maps/maps/" + getMapFileName(mapName) + ".png";
        }
        
        function getMapFileName(mapName) {
        	switch (mapName) {
        	case "TowersOfDoom": {
        		return "towers-of-doom";
        	}
        	case "BraxisHoldout": {
        		return "braxxis_preview";
        	}
			case "BlackheartsBay": {
				return "blackhearts-bay";		
			}
			case "BattlefieldOfEternity": {
				return "battlefield-of-eternity";
			}
			case "Crypts": {
				return "tomb-of-the-spider-queen";
			}
			case "ControlPoints": {
				return "sky-temple";
			}
			case "Shrines": {
				return "infernal-shrines";
			}
			case "CursedHollow": {
				return "cursed-hollow";
			}
			case "HauntedWoods": {
				return "garden-of-terror";
			}
        	default : {
        		return "";
        	}
        	}
        }
    }
})();