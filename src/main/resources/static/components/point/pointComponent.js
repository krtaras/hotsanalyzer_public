(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .component('pointComponent', {
            templateUrl: 'components/point/pointTemplate.html',
            controller: PointComponentController,
            bindings: {
            	data: '=',
            	size: '=',
            	color: '@'
            },
        });

    PointComponentController.$inject = ['GameDataService'];
    function PointComponentController(GameDataService) {
        var $ctrl = this;
        
        $ctrl.getX = getX;
        $ctrl.getY = getY;
        
        function getX(item) {
        	if (item.positionX) {
        		item.x = item.positionX * 256 / 1048576;
        	}
        	if (item.x) {
        		var x = ((item.x+3) * $ctrl.size.mapWidth) / 256;
        		return parseInt(x);
        	}
		}
        
        function getY(item) {
        	if (item.positionY) {
        		item.y = item.positionY * 256 / 1048576;
        	}
        	if (item.y) {
        		var y = ((item.y+13) * $ctrl.size.mapHeight) / 240;
        		return parseInt($ctrl.size.mapHeight-y);
        	}
		}
        
        /*function getX(item) {
        	if (item.positionX) {
        		item.x = item.positionX * 256 / 1048576;
        	}
        	if (item.x) {
        		var x = ((item.x-8) * $ctrl.size.mapWidth) / 235;
        		return parseInt(x+2);
        	}
		}
        
        function getY(item) {
        	if (item.positionY) {
        		item.y = item.positionY * 256 / 1048576;
        	}
        	if (item.y) {
        		var y = ((item.y - 32) * $ctrl.size.mapWidth) / 245
        		return parseInt($ctrl.size.mapHeight-y+14);
        	}
		}*/
    }
})();