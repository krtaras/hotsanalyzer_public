(function() {
    'use strict';

    angular
        .module('hots-analyzer')
        .directive('mapSizeDirective', MapSizeDirective);

    MapSizeDirective.$inject = ['$window', 'GameDataService'];
    
    function MapSizeDirective($window, GameDataService) {
        var directive = {
            link: link,
            restrict: 'A',
        };
        return directive;
        
        function link(scope, element, attrs) {
        	scope.onResize = function() {
                console.log(element[0].width);
                scope.vm.setSize(element[0].width, element[0].height);
             }
             scope.onResize();

             angular.element($window).bind('resize', function() {
                 scope.onResize();
             })
             element.bind('load', function() {
            	 scope.onResize();
             });
        }
    }
})();