(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .directive('mapDirective', MapDirective);
    
    MapDirective.$inject = ['$window'];
    function MapDirective($window) {
    	var directive = {
            link: link,
            templateUrl: 'components/map/mapTemplate.html',
            restrict: 'E',
            scope: {
            	map: '@',
            	data: '=',
            	moves: '='
            },
            controller: MapController,
            controllerAs: 'vm',
            bindToController: true
        };
    	return directive;
    	
    	function link(scope, element, attrs, vm) {
        	scope.onResize = function() {
                console.log(element.find('img')[0].width);
                vm.size.mapWidth = element.find('img')[0].width;
            	vm.size.mapHeight = element.find('img')[0].height;
             }
             scope.onResize();

             angular.element($window).bind('resize', function() {
                 scope.onResize();
                 scope.$digest();
             });
             element.find('img').bind('load', function() {
            	 scope.onResize();
                 scope.$digest();
             });
             
        }
    	
    }
    
    MapController.$inject = ['$scope'];

    function MapController($scope) {
        var vm = this;
        vm.mapSrc = getMapSrc;
        vm.size = {
        	mapWidth: '',
        	mapHeight: ''
        }
        
        function getMapSrc() {
        	return "media/maps/maps/" + vm.map + ".png";
        }
        
        
        
    }
})();