(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .controller('GameDataController', GameDataController);

    GameDataController.$inject = ['gameData', '$scope', '$filter', 'GameDataService'];
    function GameDataController(gameData, $scope, $filter, GameDataService) {
    	var fixedData = angular.copy(gameData);
        var vm = this;
        vm.gameData = gameData;
        vm.map = getMapFileName(fixedData.mapName);
        vm.selectFilter = {
        	playerId: -1,
        	action: '',
        	itemNumber: 0
        }
        vm.movePoints = [];
        
        vm.playerList = GameDataService.getPlayersLoginHeroList(fixedData);
        vm.actions = [
                      	{ key: 'KU', value: 'Killed units'}, 
                      	{ key: 'KP', value: 'Killed players'},
                      	{ key: 'D',  value: 'Death'}
        ];
        vm.gridOnTop = false;
        
        vm.filterDisplayData = filterDisplayData;
        vm.toggleKilledPlayers = toggleKilledPlayers;
        vm.toggleKilledUnits = toggleKilledUnits;
        vm.togglePlayerDeaths = togglePlayerDeaths;
        
        function toggleKilledPlayers(player) {
        	var index = vm.playerList.indexOf(player);
         	vm.playerList[index].display.killedPlayers =! player.display.killedPlayers;
        	filterDisplayData();
        }
        function toggleKilledUnits(player) {
        	var index = vm.playerList.indexOf(player);
         	vm.playerList[index].display.killedUnits =! player.display.killedUnits;
        	filterDisplayData();
        }
        function togglePlayerDeaths(player) {
        	var index = vm.playerList.indexOf(player);
         	vm.playerList[index].display.deaths =! player.display.deaths;
        	filterDisplayData();
        }
        
        function filterDisplayData() {
        	GameDataService.getFilteredDataByPlayerDisplaySettings(vm.gameData, fixedData, vm.playerList);
        }
        filterDisplayData();
        
        vm.loaded = true;
        
        vm.selectChange = function() {
        	if (vm.selectFilter.playerId >=0 ) {
        		vm.getMaxLoop = fixedData.gameDataDetails.players[vm.selectFilter.playerId-1].movePoints.length;
        	} else {
        		vm.getMaxLoop = 0;
        	}
        	
        }
        
        vm.filterData = function() {
        	if (vm.selectFilter.playerId != -1) {
        		vm.movePoints = filterByOrder(fixedData.gameDataDetails.players[vm.selectFilter.playerId-1].movePoints);
        	} else {
        		vm.movePoints = [];
        	}
        }
        
        vm.gameLoop = 0;
        vm.getMinLoop = 0;
        vm.getMaxLoop = 0;
        
        var filterByOrder = function(fixedList) {
        	return $filter('listIndexFilter')(fixedList, vm.gameLoop, false);
        }
        
        function getMapFileName(mapName) {
        	switch (mapName) {
        	case "TowersOfDoom": {
        		return "towers-of-doom";
        	}
        	case "BraxisHoldout": {
        		return "braxxis_preview";
        	}
			case "BlackheartsBay": {
				return "blackhearts-bay";		
			}
			case "BattlefieldOfEternity": {
				return "battlefield-of-eternity";
			}
			case "Crypts": {
				return "tomb-of-the-spider-queen";
			}
			case "ControlPoints": {
				return "sky-temple";
			}
			case "Shrines": {
				return "infernal-shrines";
			}
			case "CursedHollow": {
				return "cursed-hollow";
			}
			case "HauntedWoods": {
				return "garden-of-terror";
			}
        	default : {
        		return "";
        	}
        	}
        }
    }
})();