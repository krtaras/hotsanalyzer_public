(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .filter('listIndexFilter', ListIndexFilter);
    
    ListIndexFilter.$inject = ['$filter'];

    function ListIndexFilter($filter) {
        return ListIndexFilter;

        function ListIndexFilter(items, index, onlyOne) {
            if (onlyOne) {
            	return items[index];
            } else {
            	return items.slice(index-1, index);
            }
        }
    }
})();