(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .service('GameDataService', GameDataService);

    GameDataService.$inject = ['$filter'];
    function GameDataService($filter) {
    	var vm = this;
    	
    	
    	vm.getPlayersLoginHeroList = getPlayersLoginHeroList;
    	vm.getFilteredDataByPlayerDisplaySettings = getFilteredDataByPlayerDisplaySettings;
    	
    	
    	function getPlayersLoginHeroList(gameData) {
			var result = [];
			for (var i in gameData.teamOne.players) {
				result.push(getPlayerData(gameData.teamOne.players[i], false));
			}
			for (var i in gameData.teamTwo.players) {
				result.push(getPlayerData(gameData.teamTwo.players[i], true));
			}
			return result;
		}
    	
    	function getPlayerData(player, redTeam) {
    		return {
				id: player.playerId,
				login: player.login,
				hero: player.hero.replace('Hero', ''),
				redTeam: redTeam,
				numbers: {
					killedUnits: player.killedUnits != null ? player.killedUnits.length : [],
					killedPlayers: player.killedPlayer != null ? player.killedPlayer.length : [],
					deaths:	player.killedByPlayers != null ? player.killedByPlayers.length : []
				},
				talents: player.selectedTalents,
				display: {
					killedUnits: false,
					killedPlayers: true,
					deaths: false
				}
			};
    	}
    	
    	function getFilteredDataByPlayerDisplaySettings(gameData, fixedData, playersList) {
    		var displaySettings;
    		for (var i in gameData.teamOne.players) {
    			i = parseInt(i);
    			var player = gameData.teamOne.players[i];
				var fixedPlayer = fixedData.teamOne.players[i];
    			displaySettings = getPlayerDisplaySettingsById(player.playerId, playersList);
				updatePlayerDataByFilter(player, fixedPlayer, displaySettings);
			}
			for (var i in gameData.teamTwo.players) {
				i = parseInt(i);
				var player = gameData.teamTwo.players[i];
				var fixedPlayer = fixedData.teamTwo.players[i];
    			displaySettings = getPlayerDisplaySettingsById(player.playerId, playersList);
				updatePlayerDataByFilter(player, fixedPlayer, displaySettings);
			}
			console.log('b');
    	}
    
    	function updatePlayerDataByFilter(player, fixedPlayer, filter) {
    		if (filter.killedUnits) {
				player.killedUnits = fixedPlayer.killedUnits;
			} else {
				player.killedUnits = [];
			}
			
			if (filter.killedPlayers) {
				player.killedPlayer = fixedPlayer.killedPlayer;
			} else {
				player.killedPlayer = [];
			}
			
			if (filter.deaths) {
				player.killedByPlayers = fixedPlayer.killedByPlayers;
			} else {
				player.killedByPlayers = [];
			}
    	}
    	
    	function getPlayerDisplaySettingsById(playerId, playersList) {
    		return $filter('filter')(playersList, {id : playerId})[0].display;
    	}
    }
})();