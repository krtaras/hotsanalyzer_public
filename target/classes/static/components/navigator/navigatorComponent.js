(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .component('navigatorComponent', {
            templateUrl: 'components/navigator/navigatorTemplate.html',
            controller: NavigatorController,
            bindings: {
            },
        });

    NavigatorController.$inject = ['$state'];
    function NavigatorController($state) {
        var $ctrl = this;
    }
})();