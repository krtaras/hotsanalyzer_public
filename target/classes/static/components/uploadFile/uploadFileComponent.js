(function() {
'use strict';
    angular
        .module('hots-analyzer')
        .component('uploadFileComponent', {
            templateUrl: 'components/uploadFile/uploadFileTemplate.html',
            controller: UploadFileController,
            bindings: {
            },
        });

    UploadFileController.$inject = ['$http', 'Upload', 'LockScreenService', '$state', 'UserAuthenticationService'];
    function UploadFileController($http, Upload, LockScreenService, $state, UserAuthenticationService) {
        var $ctrl = this;
        $ctrl.$file = null;
        $ctrl.fileNmae = '';
        $ctrl.fileUploadSelection = fileUploadSelection;

		function fileUploadSelection($file) {
			if ($file) {
				LockScreenService.lockScreen();
				Upload.upload({
					url : 'filemamager/upload',
					data : {
						file: $file,
						user: UserAuthenticationService.getCurrentUser() != null ? UserAuthenticationService.getCurrentUser().id : ''
					}
				}).then(
					function(data, status, headers, config) {
						$ctrl.$file = null;
						$state.go('game', {"id": data.data.body});
						LockScreenService.resolveScreen();
					},
					function(resp) {
						console.log('Error status: ' + resp.status);
						$ctrl.$file = null;
						LockScreenService.resolveScreen();
					},
					function(evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
					}
				)
			}
		}
    }
})();