(function() {
'use strict';

    angular
        .module('hots-analyzer')
        .service('LockScreenService', LockScreenService);
    
    function LockScreenService() {
        this.lockScreen = lockScreen;
        this.resolveScreen = resolveScreen;
        
        function lockScreen() {
        	$('body').append(getLockHTML(getContent()));
        }
        function resolveScreen() { 
        	$('#lockPageScreen').remove();
        }
	    
		var getContent = function() {
			return '<div class="spinner"> \
				        <div class="rect-1"></div> \
				        <div class="rect-2"></div> \
				        <div class="rect-3"></div> \
				        <div class="rect-4"></div> \
				        <div class="rect-5"></div> \
				        <div class="rect-6"></div> \
				        <div class="rect-7"></div> \
				        <div class="rect-8"></div> \
				    </div>';
		}
		
		var getLockHTML = function(content) {
			var div = $('<div id="lockPageScreen"></div>');
			div.addClass('lock');
			var container = $('<div></div>');
			container.addClass('lock-container');
			container.append(content);
			div.append(container);
			return div;
		}
    }
})();


var UtilHelpers = new (function(){
	
	var LOADING = "LOADING";
	var SAVING = "SAVING";
	
	this.lockType = {
		LOADING: function() {
			return LOADING;
		},
		SAVING: function() {
			return SAVING;
		}
	}
	
	this.lockScreen = function(type) {
		var lockContent = '';
		switch(type) {
			case LOADING : {
				lockContent = getLoandingContent();
				break;
			}
			case SAVING : {
				lockContent = getSavingContent();
				break;
			}
			default : {
				break;
			}
		}
		$('body').append(getLockHTML(lockContent));
	}
	
	this.resolveScreen = function() {
		$('#lockPageScreen').remove();
	}
	
	var getLoandingContent = function() {
		return getSavingContent();
	}
	
	var getSavingContent = function() {
		return '<div class="spinner"> \
			        <div class="rect-1"></div> \
			        <div class="rect-2"></div> \
			        <div class="rect-3"></div> \
			        <div class="rect-4"></div> \
			        <div class="rect-5"></div> \
			        <div class="rect-6"></div> \
			        <div class="rect-7"></div> \
			        <div class="rect-8"></div> \
			    </div>';
	}
	
	var getLockHTML = function(content) {
		var div = $('<div id="lockPageScreen"></div>');
		div.addClass('lock');
		var container = $('<div></div>');
		container.addClass('lock-container');
		container.append(content);
		div.append(container);
		return div
	}
})();