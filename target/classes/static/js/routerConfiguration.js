(function() {
'use strict';
	angular
	.module('hots-analyzer')
	.config(function($stateProvider, $urlRouterProvider) {
	    
	    $urlRouterProvider.otherwise('/');
	    
	    $stateProvider
	       	.state('home', {
	       		url: '/',
	            templateUrl: 'view/home.html'
	       	})
	        .state('game', {
	            url: '/game/:id',
	            templateUrl: 'view/gameStats.html',
	            controller: 'GameDataController',
	            controllerAs: 'gameDataCtrl',
	            resolve: {
	            	gameData:['$http', '$stateParams', function($http, $stateParams) {
			            	return $http.get('game/'+$stateParams.id)
			                .then(function(response) {
			                	return response.data;
			                });
		            	}]
	            	}
	        })
	        .state('mypage', {
	        	url: '/mypage/',
	            templateUrl: 'view/mypage.html'
	        })
	});
})();