<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="hots-analyzer">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hots Analyzer</title>

<script src="/webjars/angularjs/1.5.8/angular.js"></script>
<script src="/webjars/angularjs/1.5.8/angular-resource.js"></script>
<script src="/webjars/angular-ui-router/1.0.0-beta.1/angular-ui-router.js"></script>
<script src="/webjars/jquery/1.11.1/jquery.js"></script>
<script src="/webjars/bootstrap/4.0.0-alpha.3/js/bootstrap.js"></script>
<script src="/js/lib/ng-file-upload.min.js"></script>

<script src="/js/lib/bootstrap-select.min.js"></script>

<script src="/js/app.js"></script>
<script src="/js/routerConfiguration.js"></script>

<script src="/components/navigator/navigatorComponent.js"></script>
<script src="/components/user/userAuthenticationService.js"></script>
<script src="/components/user/userController.js"></script>

<script src="/components/uploadfile/uploadFileComponent.js"></script>
<script src="/js/helperServices/lockScreeService.js"></script>

<script src="/components/map/gameDataController.js"></script>
<script src="/components/map/mapComponent.js"></script>
<script src="/components/map/mapSizeDirective.js"></script>
<script src="/components/map/mapDataService.js"></script>
<script src="/components/point/pointComponent.js"></script>
<script src="/components/map/listIndexFilter.js"></script>
<script src="/components/gamelist/gameListComponent.js"></script>

<link rel="stylesheet" href="/webjars/bootstrap/4.0.0-alpha.3/css/bootstrap.css">
<link rel="stylesheet" href="/webjars/font-awesome/4.6.3/css/font-awesome.css">
<link rel="stylesheet" href="/css/theme.css">
<link rel="stylesheet" href="/css/custom.css">
<link rel="stylesheet" href="/css/bootstrap-select.min">


</head>
<body>
	<div class="page">
		<navigator-component></navigator-component>
		<div class="container ">
			<div ui-view></div>
		</div>
	</div>
	<!-- <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>
 -->
</body>
</html>